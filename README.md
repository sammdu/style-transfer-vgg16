## Style Transfer with VGG16
![Demonstration](https://raw.githubusercontent.com/sammdu/style-transfer-vgg16/light/demo/male-abstract-1.png)   
   
This is a style transfer program created with ![Tensorflow](https://www.tensorflow.org) as well as a pre-trained VGG16 model.
